import Vue from "vue";
import VueRouter from "vue-router";
import Film from '../views/Film';
import Nowplaying from '../views/Film/Nowplaying';
import Comingsoon from '../views/Film/Comingsoon';
import Cinemas from '../views/Cinemas';
import Cinema from '../views/Cinema';
// import Chedule from '../views/Cinema/Schedules'
import CinemaDetail from '../views/Cinema/CinemaDetail';
import Center from '../views/Center';
import Detail from '../views/Detail';
import City from '../views/City';

Vue.use(VueRouter);

// 解决"避免到当前位置的冗余导航"的报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/film',
    component: Film,
    children: [  // 将nowplaying和comingsoon设为film的二级路由
      {
        path: '/film/nowplaying',
        component: Nowplaying
      },
      {
        path: '/film/comingsoon',
        component: Comingsoon
      },
      {
        // 当地址/film后面没有输入地址，重定向到/film/nowplaying
        path: '', // 当地址/film后面为空时，则进行重定向
        redirect: '/film/nowplaying'
      }
    ]
  },
  {
    path: '/cinemas',
    component: Cinemas,
  },
  {
    path: '/cinema/:id/film', // 影院页
    name: 'cinema',
    component: Cinema,
    children: [
      {
        path: '/cinema/:id/film/:filmid', // 匹配带有影片id的地址
        name: 'schedules',
        // component: Chedule,
        children: [
          {
            path: '/cinema/:id/film/:filmid/:dataid', // 匹配带有排期id的地址
            // component: Cheduleslist
            name: 'schedule',
          },
        ]
      },
    ]
  },
  {
    path: '/cinema/:id',
    component: CinemaDetail
  },
  {
    path: '/center',
    component: Center
  },
  {
    // 可使页面打开后默认显示film模块
    path: '*',  // 如果不匹配以上设置的路径，则重定向到film
    redirect: '/film'
  },
  {
    // 通过动态路由匹配地址
    path: '/detail/:id',  // (:id处可以匹配任意值)
    name: 'kerwindetail',
    component: Detail,
    props: true // 开启props传输id
  },
  {
    path: '/city',
    name: 'city',
    component: City
  }
];

const router = new VueRouter({
  // mode: "history",
  // base: process.env.BASE_URL,
  routes
});

export default router;
