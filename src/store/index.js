import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios';
import { Indicator } from "mint-ui"; // 页面加载期间动画效果
// 引入mutation常量名
import { HIDE_TABBAR_MUTATION, SHOW_TABBAR_MUTATION, NOWPLAYING_DATA_MUTATION, COMINGSOON_DATA_MUTATION } from '../type'

Vue.use(Vuex);

export default new Vuex.Store({
  // 自定义的共享状态
  state: {
    isTabbarShow: true,  // tabbar显示状态
    Nowplayingdatalist: [], // Nowplaying缓存数据
    Comingsoondatalist: [], // Comingsoon缓存数据
  },
  // getters是store的计算属性
  getters: {
    // 筛选出Comingsoondatalist数组前三个元素
    ComingsoondatalistGetter(state) {
      return state.Comingsoondatalist.filter((item, index) => index < 3)
    }
  },
  mutations: {
    [HIDE_TABBAR_MUTATION](state, data) {
      state.isTabbarShow = data
    },
    [SHOW_TABBAR_MUTATION](state, data) {
      state.isTabbarShow = data
    },
    [NOWPLAYING_DATA_MUTATION](state, data) {
      state.Nowplayingdatalist = ([...state.Nowplayingdatalist, ...data])
      // console.log(state.Nowplayingdatalist);
    },
    [COMINGSOON_DATA_MUTATION](state, data) {
      state.Comingsoondatalist = [...state.Comingsoondatalist, ...data]
      console.log(state.Comingsoondatalist);
    }
  },
  actions: {
    // 获取Nowplaying数据
    getNowplayingAction(store, current) {
      Indicator.open(); // 开启加载动画
      return new Promise((resolve) => {
        axios({
          url:
            `https://m.maizuo.com/gateway?cityId=110100&pageNum=${current}&pageSize=10&type=1&k=4316124`,
          headers: {
            "X-Client-Info":
              '{"a":"3000","ch":"1002","v":"5.0.4","e":"1611156413146600118714369","bc":"110100"}',
            "X-Host": "mall.film-ticket.film.list",
          },
        }).then((res) => {
          Indicator.close();  // 关闭加载动画
          // console.log(res.data.data.films);
          store.commit(NOWPLAYING_DATA_MUTATION, res.data.data.films)  // 第一个参数是mutation名字
          // 利用resolve带出正在热映影片个数，组件中通过.then调用参数
          resolve(res.data.data.total)
        }).catch(() => {
          Indicator.close(); // 关闭加载动画
        });
      })
    },
    // 获取Comingsoon数据
    getComingsoonAction(store, current) {
      Indicator.open(); // 开启加载动画
      return new Promise((resolve) => {
        // console.log(resolve, '内部');
        axios({
          url: `https://m.maizuo.com/gateway?cityId=110100&pageNum=${current}&pageSize=10&type=2&k=6483662`,
          headers: {
            'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.0.4","e":"1611750210413764264394753","bc":"110100"}',
            'X-Host': 'mall.film-ticket.film.list'
          }
        }).then(res => {
          Indicator.close();  // 关闭加载动画
          // console.log(res.data.data);
          // 通过mutations保存数据
          store.commit(COMINGSOON_DATA_MUTATION, res.data.data.films) // 第一个参数是mutation名字
          resolve(res.data.data.total)  // 通过promise返回影片总数，以便判断是否已经获取到所有数据

        }).catch(() => {
          Indicator.close(); // 关闭加载动画
        });
      })
    }
  },
  modules: {}
});
