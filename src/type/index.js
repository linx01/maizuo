// 保存mutation常量风格，防止命名冲突
export const HIDE_TABBAR_MUTATION = 'HideTabbar';
export const SHOW_TABBAR_MUTATION = 'ShowTabbar';
export const NOWPLAYING_DATA_MUTATION = 'NowplayingdataMutation';
export const COMINGSOON_DATA_MUTATION = 'ComingsoondataMutation';
